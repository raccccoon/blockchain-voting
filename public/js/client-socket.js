$(function () {

    var socket = io();

    socket.on('message', function (blockchain) {
      window.database = blockchain[0].data;
      window.blockchain = blockchain;
    });

    socket.on('validationError', function (message) {
        alert(message);
    });

    $('#doVote').click(function(){
        if(!doSign()){
            return;
        }

        var message = $('#vote').val().trim();
        var publicKey = $('#publicKey').val().trim();

        if(!publicKey) {
            alert('Уведiть публіний ключ');
            return;
        }

        var publicKeyHex = makeSha256(publicKey);
        var signature = $('#signature').val().trim();

        if(window.database.includes(publicKeyHex)){
            socket.emit('make vote', message, publicKey, signature);
        } else {
            alert('Публічний ключ не збігається з відповідним йому прихованим!');
        }

        return false;
    });

    // disable button if already voted
    socket.on('vote success', function(){
        $('#doVote').prop('disabled', true).attr('value', 'Зараховано');
    });

    // if recieved new blockchain - check if it is valid
    // and replace local blockchain
    socket.on('new blockchain', function(newBlockchain){
        if (isValidChain(newBlockchain) && newBlockchain.length > window.blockchain.length) {
            window.blockchain = newBlockchain;
            console.log('OK');
        } else {
            alert('New blockchain is not valid.');
        }
        window.blockchain = newBlockchain;
    });
}); 