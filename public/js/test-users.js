// generate with 
// openssl req -x509 -nodes -sha256 -days 365 -newkey rsa:2048 -keyout name.key -out name.crt
$('#firstUser').click(function(){
	$('#publicKey').val(`-----BEGIN CERTIFICATE-----
MIIDtTCCAp2gAwIBAgIJAJUrnCPDo070MA0GCSqGSIb3DQEBCwUAMEUxCzAJBgNV
BAYTAkFVMRMwEQYDVQQIEwpTb21lLVN0YXRlMSEwHwYDVQQKExhJbnRlcm5ldCBX
aWRnaXRzIFB0eSBMdGQwHhcNMTcwNTEzMTIwNTQwWhcNMTgwNTEzMTIwNTQwWjBF
MQswCQYDVQQGEwJBVTETMBEGA1UECBMKU29tZS1TdGF0ZTEhMB8GA1UEChMYSW50
ZXJuZXQgV2lkZ2l0cyBQdHkgTHRkMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIB
CgKCAQEAmwLIbBFxYVLO1K/6C5ngUEGy5kSnv2Wj69xzehLaq29uwxF5i4NQlvgw
YbQ3sRK99olaZPz6v5jRHx7Mc7C+pfUjOZKFElD03++SyyHinMCiGtg0FH/m1SA5
gN58GsTgRAjWFY5KBGzMQh9SLB6yv+l0RkdX4iPDP6ARpF07Jzs18w/JaL5ghe9J
06nUu9bX9FJ5DiwJ2yDz5hi88cLAOmZiXo/uK+gquZlWAZmQ5glSq8fmYM0u+3vX
37nRkz56ftkRfVqhcxX6pn79+0fq4Z7tB+IfgekXSYvycjLTgjU52DvKVPNvV9wC
3tajCAIGyLqXHPmQKNSG4DmoMHNtmwIDAQABo4GnMIGkMB0GA1UdDgQWBBTfOs2H
b+yoDYGvezAhmJAaUmT6iTB1BgNVHSMEbjBsgBTfOs2Hb+yoDYGvezAhmJAaUmT6
iaFJpEcwRTELMAkGA1UEBhMCQVUxEzARBgNVBAgTClNvbWUtU3RhdGUxITAfBgNV
BAoTGEludGVybmV0IFdpZGdpdHMgUHR5IEx0ZIIJAJUrnCPDo070MAwGA1UdEwQF
MAMBAf8wDQYJKoZIhvcNAQELBQADggEBAGbWHWJdQ4Cs4ljP9rMPmrCOzgRtBbZg
JiY/ADtD6uCqDOnpOxyLmDGNXkSpf7zjtot99IF2YFc99aXjNYc5bBXZIdVjTDav
XJsf90beWkJ4r1q0evRx9I/S5mBJUY8fQItRkK9FQ2UKGsjbipTtUojO8feNPaV9
b7FNIKfOhyKSsVeStJ+jlQd4aiUxiIjQdngt5eEODG6wmFfG0QjL+umx26y14c/P
hzOlmR2bv0vM5LQLNpY1yyVOAwWkgarsSEguCo3gK3uXbTB9Hd9VbMNk+o7KDLgR
bNHDxRY2Q/S4iepAelpUFZVJ+Ujg2HgT9DCL1ZQNxva22D6JEbYOAfA=
-----END CERTIFICATE-----`);

	$('#privateKey').val(`-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEAmwLIbBFxYVLO1K/6C5ngUEGy5kSnv2Wj69xzehLaq29uwxF5
i4NQlvgwYbQ3sRK99olaZPz6v5jRHx7Mc7C+pfUjOZKFElD03++SyyHinMCiGtg0
FH/m1SA5gN58GsTgRAjWFY5KBGzMQh9SLB6yv+l0RkdX4iPDP6ARpF07Jzs18w/J
aL5ghe9J06nUu9bX9FJ5DiwJ2yDz5hi88cLAOmZiXo/uK+gquZlWAZmQ5glSq8fm
YM0u+3vX37nRkz56ftkRfVqhcxX6pn79+0fq4Z7tB+IfgekXSYvycjLTgjU52DvK
VPNvV9wC3tajCAIGyLqXHPmQKNSG4DmoMHNtmwIDAQABAoIBAFJQEv+X754fq+cz
H1NjvcLAcHBoiq9PGNGZedScCHQ6sxVwxRs5euUAHCj12xDzg95Tp6Wy1LBDROEC
qMzSURqtZqweyv0j8jSr8kZd0wksHailoFo2ZOxNtVtiSYKpgvF9w7kkmtdosrp2
CVuWXZMdM/pRzjAuj3LM3DFstImqyuPXRlDDDbfSEV4GdkfqMkQoC6mQ2zSgJbcD
W1zVn/Y2JkNov12Ra9ZNEatgzPbfqiIrPKh5FW2EdM1Og4jtEEqdR3FDXmyoFvp/
X2tNotAnZzaaabJfjyqF8WYVDeyhqfFZ0w5LxERkzkmbjIkk7GF/qD3OinST5mbe
3l9Q6dECgYEAzGFlL6kElcYG2VSyG6J+JQUgJ6dXBid8z12pGTZlpzI7vdXIA6hm
3fluPfTYWMYWOklIolGKFOA3DUXvuodwC0W1Kbvs3tbc4prTu4ruOf49SxtRvL65
lDZxfNMST6EOrN1uvjwR4KoK5bd+5l04AyU0L8KOS649RKur0Ss2wOMCgYEAwilN
1sC8+UGCbk2cAx0JJg4Nmt65u3nTsKpp1xdBnAfdOTgg4e1CEjDSDGbdGKwnBaC9
54d8JFlicurq6gYDQdD5OYeLfvLUgHYUc9hKssfAD2mRs0i0WD1IUWolHNhClKMl
szolj3jlfh5viG8EcJ+xNBeoFBqt5L2AMy0r1ekCgYAzVNLhIYYxR8c4lnkVT752
9c6ZpwHIXeqO+msaFft5/rfaefL+WDFd1uPFU9Vylm3GvdksS2Zuz9GD1+jZ5GJT
snJJxRU88qIPl+tN0xr6HcyvLHFfAlgBzZq7SxfyP/U7nXHO/a9PyeVqxKdunEMR
JyrwQ+ERSPXCZYuFwWDDGQKBgQC0oAgxyIwEtdTJGJoSq2hfZqNxd9IBfoxYxbBA
0PnQVeCP4o90nmvg0FoX/3mgAkcrDSadxYTPq/fSQIjZvU46ZFX2FwrGxtC50R+l
xYl4WCtsKyHIT9K2ZAxq8PwloWoPLNddrLJUjnuwoE/R9LWEsIsBkyBh05ACQ+iH
s3YcQQKBgQCp17TTTA/QE4s59edPN3uxooPZRr04gdwYCW+79QenfsBZdYpRKodt
IqRRVqRoOpeT+TLhPObcpKucHScwXxESRd0kZPj1JRKDTOZhMzRqq4udNrN4DQug
EULe6+L8K+FYVybYTfM1cOXazIo2zj1+1MajNAdxROQCx4qhAOP/mQ==
-----END RSA PRIVATE KEY-----`);
});

$('#secondUser').click(function(){
	$('#publicKey').val(`-----BEGIN CERTIFICATE-----
MIIDtTCCAp2gAwIBAgIJAPRkICdQv7+SMA0GCSqGSIb3DQEBCwUAMEUxCzAJBgNV
BAYTAkFVMRMwEQYDVQQIEwpTb21lLVN0YXRlMSEwHwYDVQQKExhJbnRlcm5ldCBX
aWRnaXRzIFB0eSBMdGQwHhcNMTcwNTEyMDk0MjI5WhcNMTgwNTEyMDk0MjI5WjBF
MQswCQYDVQQGEwJBVTETMBEGA1UECBMKU29tZS1TdGF0ZTEhMB8GA1UEChMYSW50
ZXJuZXQgV2lkZ2l0cyBQdHkgTHRkMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIB
CgKCAQEAvAYGK4gHgKlp3T87RJXe5R+8RR7zzYMSCbvY5BogexPwcDZ+fQi79tCc
FghhU4cXK7GZanKYHcaUpybGnnGttYOBIbKvgpPwK3j/hHQ/JeYP3s6W0YpXWGwy
XacnoaVjEO2b1Ce0LOu5/3ftFOZwzfOiv+L5AfDK8RrjR85z86LfLoQcPfNUMncB
TrnFkskHhYphZdNHaaDYzniPqs+UNl1e98FpPqXEUWsQ7NtBEmiUj/bqbquWakiY
VWzi5Ju4C8yik1pt/4qE4/7U5gRNI/GCDFJEAbNE9qgtyMF61GvG5dQy48BRawEP
ocjm5xhpl0ouwhsprc9pSHoGdonkxQIDAQABo4GnMIGkMB0GA1UdDgQWBBTXtdsd
N+3UCKpoMZYeDCanHWUsSjB1BgNVHSMEbjBsgBTXtdsdN+3UCKpoMZYeDCanHWUs
SqFJpEcwRTELMAkGA1UEBhMCQVUxEzARBgNVBAgTClNvbWUtU3RhdGUxITAfBgNV
BAoTGEludGVybmV0IFdpZGdpdHMgUHR5IEx0ZIIJAPRkICdQv7+SMAwGA1UdEwQF
MAMBAf8wDQYJKoZIhvcNAQELBQADggEBABef90YhtMcWMHdaRx/Aq2MWY4Dq3x4+
EC/uLvFlNHkyYz9MxCYr2wWVSqzKwzol2KuOxKLt2CHsbS/oNgMgG0UJiT76rHhl
DkGfwyd7ZnjsoVsu0sWqeGF2uctOBWiHOmAIer5Zdb/XwThHPkF61EAehEy/WPbf
UVtuNpKDWMw8YEhmAbaOsLPH8qyUKDz1NGPotk/8d0PO2pZkNrHnpxtyW3ddS3Rr
THNcTMz9aFa/VEEDtP7Us1PrgrF/tNYbfD38p7fakKVxoWMsR08lkEzVc0BKUbr+
qp7oxNaTWn3sk1ZGwK9YCeeAvaYjZxrUayYGP2uY/Vkbf4oPdjrm/aQ=
-----END CERTIFICATE-----`);

	$('#privateKey').val(`-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEAvAYGK4gHgKlp3T87RJXe5R+8RR7zzYMSCbvY5BogexPwcDZ+
fQi79tCcFghhU4cXK7GZanKYHcaUpybGnnGttYOBIbKvgpPwK3j/hHQ/JeYP3s6W
0YpXWGwyXacnoaVjEO2b1Ce0LOu5/3ftFOZwzfOiv+L5AfDK8RrjR85z86LfLoQc
PfNUMncBTrnFkskHhYphZdNHaaDYzniPqs+UNl1e98FpPqXEUWsQ7NtBEmiUj/bq
bquWakiYVWzi5Ju4C8yik1pt/4qE4/7U5gRNI/GCDFJEAbNE9qgtyMF61GvG5dQy
48BRawEPocjm5xhpl0ouwhsprc9pSHoGdonkxQIDAQABAoIBABIoXsYrb9YMBk44
ggfAlXL5GpxuPcs2AjLwciStJT/dkBW2HYcDpetTAA/cpVGEtUch8pfmEyyk9cwi
oYHBpq3l0j2n3O02TjZUjBCFjWeijRvORaBcxMykPkTvFJO2vhdvauIVJ4BDD4ZN
VZb0Hia1hZ0UVUnLsD2ShCp2FJqNkIDCZSgIbZAmoITRvzu7uzj5NTe/UXYIKO1c
2gEaxRsYi7IPy10D5ECWSegc5NKJBFxxw7ZB+HbtMpMQluka9pnd24uHFLwjZRif
OB095XozbBeRZ0nrkA4uAnXI6GDzwq6UGQE4OXYBdVL+5OHd3Thc6yvGGPXyOCqV
Ehvk7W0CgYEA8VPvnXg+RdZ6Oipl90opAN/u7OUEVaAF85FgTLXHT1U4ZUrC3fKR
0iZK7McYnQn9XNOhHWXYKRLbOAQNKzB3Kr/PN3Hm02VatN7hikIFFkqLT+V1erO1
SoHSD49ANVik/lfHRTgLzP/OQtJmV9vajphBsvNwKhskm++HjC4ueA8CgYEAx3Rz
fhE96RUnbJl/H88T/odVjUzPYYh/eQ6/3cGXNNOmDj1WBH1A1U8ku32KyJ8jJGxI
NC2GlfTJVh06li8crSMJiVSNrsb/q0q2w8QShQhdPrD5jRqWeJYAcScgWYLjzMnN
XB+U29wNvFRAD5mCkwdcJoNsJtjMSgS5LJ9cYesCgYEA8S8r+LrlsbJqsZsSERVi
6WbkSImRzWmCySxvi/rYWOcY19Lh8iOtrNNsajuI09naB13SVaklYddglBkoeBOV
5XmRRR5mzLss7jsaOa1FnEOf+5ed06Wq9+3K3JYUW7xeGXvswbcDxDucDphshD5L
rdtpPcmiPdKmwdmgJG87Le0CgYBmx0g4P1RCxobRPGu4jXHFKCHatSjsHnVpgUM3
V2m+bA3Wqp6no4+EG8tW3gNlDDQEsdOnJ4qBvWj+egNVnqNNSI/Cd5Km7J6UuuRh
Dqloaf36ryF5J6lALDPAGyRdW8hdH+Q1w4Rmowdk85krMLSLZTCEHBbgEbl7ruFo
8BmfzwKBgEafoQoAgCVuw66vkEhoruUwmkYniML3nET+2kgwORQbEVkUfjcGQPAf
KO7kIgBnME3fajFihBJUx9Qb5+pJQvjv8Y6cA6fOyOkcia1mVZBulTrfWahtbsKG
QpLUSp3EtKz/TbpiCN668kY1kRJ/m6MmfuOwUWGyRAN3jx5P71d2
-----END RSA PRIVATE KEY-----`);
});

$('#thirdUser').click(function(){
	$('#publicKey').val(`-----BEGIN CERTIFICATE-----
MIIDtTCCAp2gAwIBAgIJAJRu5LzwKj15MA0GCSqGSIb3DQEBCwUAMEUxCzAJBgNV
BAYTAkFVMRMwEQYDVQQIEwpTb21lLVN0YXRlMSEwHwYDVQQKExhJbnRlcm5ldCBX
aWRnaXRzIFB0eSBMdGQwHhcNMTcwNTEyMjIxODIzWhcNMTgwNTEyMjIxODIzWjBF
MQswCQYDVQQGEwJBVTETMBEGA1UECBMKU29tZS1TdGF0ZTEhMB8GA1UEChMYSW50
ZXJuZXQgV2lkZ2l0cyBQdHkgTHRkMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIB
CgKCAQEAu6s0F0cune0veeLR88Kr8KkLbMwGAWQHe2FrlueF6Qk33sy6YrG0jJsZ
bwKjPErheWwRwrkBaS9gcAXuH5iDiX4QgBAWsHjbHPirYOyEJkmLhw3xC914IfPm
oKXsD0egYfotCfOSostP4cHVavHRUF4HQZuFLM8j5pKcDmkfUBBRWF4Ti8HXf2RA
2gyDjtwSKXrMwM7LgntE/Fn3tTTy+cmj+OBZFqqPOyvJqKiNRlyXmvIqGmiNIYWH
CzxHo1kkQr4+loBy0J8iDJxIn24o9Q2TZyRr62Jp9oyy19liXmAdbSBmfgxp2H8b
Ozqed2Zttf9Y+puaDwRuui2QxI3nsQIDAQABo4GnMIGkMB0GA1UdDgQWBBSbiAWa
WkVIW7WUmWm6Cm0FwO7FhTB1BgNVHSMEbjBsgBSbiAWaWkVIW7WUmWm6Cm0FwO7F
haFJpEcwRTELMAkGA1UEBhMCQVUxEzARBgNVBAgTClNvbWUtU3RhdGUxITAfBgNV
BAoTGEludGVybmV0IFdpZGdpdHMgUHR5IEx0ZIIJAJRu5LzwKj15MAwGA1UdEwQF
MAMBAf8wDQYJKoZIhvcNAQELBQADggEBABAbOg9AdG200ehQIolF8oCRruFGddde
jS0I3hhRR9y9DWy4WcrAN0rGMi3KNZcd6ObbHgwWmNOBGjyzT94XoZrVhWKl0JPB
mhnG1Zewqm1rLEeQCjEN6+tckSkn93RombyCabtyRwouhmqjIjJdrdwXcbTfTWwS
PYWnVaSoqOiOV59cwzJSrCmrkfIPyh0UuS1lbJHWX61AYfi9CGHUIz50aJxgE+C5
kSgrLdmLZDS/DC61aqtmCH+xLO/TrlEHiE7Utj4tEjVx1rb0OK9fMcP4atutFeY5
6N+fiyEhQZxQrFjJCSWuB9/e42gxv0rQNhFIkVUsoopdweKelra8E/c=
-----END CERTIFICATE-----`);

	$('#privateKey').val(`-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEAu6s0F0cune0veeLR88Kr8KkLbMwGAWQHe2FrlueF6Qk33sy6
YrG0jJsZbwKjPErheWwRwrkBaS9gcAXuH5iDiX4QgBAWsHjbHPirYOyEJkmLhw3x
C914IfPmoKXsD0egYfotCfOSostP4cHVavHRUF4HQZuFLM8j5pKcDmkfUBBRWF4T
i8HXf2RA2gyDjtwSKXrMwM7LgntE/Fn3tTTy+cmj+OBZFqqPOyvJqKiNRlyXmvIq
GmiNIYWHCzxHo1kkQr4+loBy0J8iDJxIn24o9Q2TZyRr62Jp9oyy19liXmAdbSBm
fgxp2H8bOzqed2Zttf9Y+puaDwRuui2QxI3nsQIDAQABAoIBAENJrVGzWgmR43Xm
V7T41cFiOxB1Tqns/anFUlg3BxJ5Faky6pk8cin/6mydG34gsxtgHjYdM15NeFxy
I+D8RB1mL/Ba1GfNzXJjzr7fzroToI15+DVy4QH7e3OaSjtoSR8YHlH1HVx1XVzq
rOS9c3lHXX6ujl90ncCsTHv2QR3pPknRnJmYWI4IuRqWN2NWziDgX1lkO8Lwdo6E
8ynW/VR05MwZqB0WXWLwajasdAcibg+QwbXvFKOcKRm0dztZDlUJp7AXuIIIaYSK
XfcMUjeq8Vl8btMtHcfIvuJ2FRvXvKRClI/4EofNA26cLa1YgF8VyAAEdv0HfQip
w145IGkCgYEA66TafwnPG/EdJOw+A7xqefubAzUiS+pkNT3W0LlrQ9AVG8DkV/Dr
7vmiJzcDbihfDExdr+A8aMKpTVcYjE3IVLrC0yHAGX2/ClOLMA5jL7xIki5Y+E8h
FXn9KUADSv11eV8/XZ0noOPz4IImgGuXeX7fqGaEs2OHiBkSv0p9MfcCgYEAy+Fn
JAp9dk5QGHjUE9ypJADUpo4Nqkm2WGDYrYtavkTO8tf6CofKUlLzBAwULEKtoWxF
BUGBuS64LbMIdqKhat1UgebNSPwOv30PlkYxuPdCR+w68WcteEEi2KF6c3tb+35t
m+IsHfSzjCy3r1dIq7/TrXb+9zabJLoAQcy6SZcCgYAsbyucZPce57p9KecPwUm6
ss0qL6zF7+7xczW8qhol8WjpkNf+FToIGB6RkngdH5O8t/G6pmf9wSc3pELGwiwS
fh0avvSAWgvaIKLQbwYZDEIK/p5cRF+SsywfyShVzbGHL4S9XFMKrbFhFmm4MZGg
oWH/WUWQpiX6be/xDlTJRQKBgQDIRy0lt3RV0kpWL+PNexdgebxrvOHppS2OFdzk
Zh+HB1rwLX+/9nKmejulfyQV8o5UI2KN7jK7sgxVQCkROWMt8ixAy3RfvNPUActd
CKlxlNs0gffJCl0dY4cSnVYHuWe7Xw/XcM97wdTa31a11R1bMj6DiYCfYrH22wzD
lpRtEwKBgArZWcbo5BmL4JD50at8fafrZbVrxHJQT/C+qmdwdv5YpFflyPWgGbX0
nOIWkUQMwVatly6MDTJ2N2pS53RTxcDpnp5XKc7+q4tGlhVeg6cTUto3CQolafKm
ePmRiq1pJBfGY7/RlCGE0kQAU6GuTbARpLIXXuxub+DxHHxumBw6
-----END RSA PRIVATE KEY-----`);
});