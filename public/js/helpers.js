// compare 2 arrays
Array.prototype.isTheSameAs = function(array) {
  if (!array) {
    return false;
  }
  if (this.length !== array.length) {
    return false;
  }
  for (var i = 0, l = this.length; i < l; i++) {
    if (this[i] instanceof Array && array[i] instanceof Array) {
      if (!this[i].isTheSameAs(array[i])) {
        return false;
      }
    }
    else if (this[i] !== array[i]) {
      return false;
    }
  }
  return true;
}


// check if first block is the same as local
// after that, we can check all other blocks hashes
// and be sure, they are right blocks
// because all of them are connected
var isValidChain = (newBlockchain) => {
    if (!(newBlockchain[0].data.isTheSameAs(window.database))) {
        return false;
    }

    for (var i = 1; i < newBlockchain.length; ++i) {
        if (!newBlockchain[i].isValid(newBlockchain[i-1])) {
            return false;
        }
        
    }
    return true;
}
// sign message using SJCL(Stanford JavaScript Crypto Library) provider sample
function doSign() {
    var privateKeyText = $('#privateKey').val().trim();
    var vote = $('#vote').val().trim();
    var signature = $('#signature');

    if(!privateKeyText) {
        alert('Уведiть прихований ключ');
        return false;
    }

    if(!vote) {
        alert('Оберіть за кого віддаєте свій гoлос');
        return false;
    }

    try {
        var privateKey = KEYUTIL.getKey(privateKeyText);

        var sig = new KJUR.crypto.Signature({alg: 'SHA256withRSA'});
        sig.init(privateKey);
        sig.updateString(vote);

        var sigHex = sig.sign();
        signature.val(sigHex);
        return true;
    }
    catch(err) {
        alert('Неправильний прихований ключ!');
        return false;
    }
}