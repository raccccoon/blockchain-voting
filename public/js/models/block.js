class Block {
    constructor(index, previousHash, data, hash, nonce) {
        this.index = index;
        this.previousHash = previousHash;
        this.data = data;
        this.hash = hash;
        this.nonce = nonce;
    }
	isValid (previousBlock) {

	    if (previousBlock.index + 1 !== newBlock.index) {
	        return false;
	    } 

	    if (previousBlock.hash !== newBlock.previousHash) {
	        return false;
	    }

	    var calculatedNewBlockHash = makeSha256(newBlock.index + newBlock.previousHash + JSON.stringify(newBlock.data) + newBlock.nonce);

	    if (calculatedNewBlockHash !== newBlock.hash) {
	        return false;
	    }

	    if(calculatedNewBlockHash.slice(-4) !== '0000'){
	        return false;
	    }
	    return true;
	}
}
