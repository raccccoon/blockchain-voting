// index
$('body').on('click', '.btn-vote', function(){
	var name = $(this).data('name');
	var id = $(this).data('id');

	$('#vote').val(id);
	$('#confirmModal .modal-title').text('Віддати голос за кандидата ' + name)
});

data.applicants.forEach(function(applicant) {

	$('.applicants').append(`
		<div class="col-md-4 col-sm-12 applicant">
  			<div class="photo">
  				<img src="/img/` + applicant.image + `">
  			</div>
  			<div class="name text-center">
  				<h2>` + applicant.name + `</h2>
  			</div>
  			<div class="description">` + applicant.description + `</div>
  			<div class='text-center btn-vote-container'>
  				<button class='btn btn-success btn-vote' data-toggle='modal' data-target='#confirmModal' data-id='` + applicant.id + `' data-name='` + applicant.name + `'>Проголосувати</button>
  			</div>
  		</div>
  	`);

});

// results
var recievedVotesAmount = 0;
var allVotesAmount = 0;

var countVotes = (applicants, blockchain) => {
	for(var i = 1; i < blockchain.length; i++) {
    	for(var j = 0; j < blockchain[i].data.length; j++) {
    		++applicants[blockchain[i].data[j].message].votes;
    		++recievedVotesAmount;
    	} 
    }
}