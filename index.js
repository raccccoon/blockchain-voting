var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var rsa = require('jsrsasign');
var sha256 = require('sha256');

app.use(express.static(__dirname + '/public'));
app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});
app.get('/results', function(req, res){
  res.sendFile(__dirname + '/results.html');
});
app.get('/mine', function(req, res){
  res.sendFile(__dirname + '/mine.html');
});

// users that are allowed to vote (sha-256(public key))
var dataBase = [
	'cff554318aff42c981868342f1f3cd796de15dda4f230d46a0ed4736fabf2d05',
	'083c7302631c01d1b9a68331e3d59bbf49cfbad4624fe7513a5003494ebfc9eb',
	'6e9b751cd391fbac6957737af60cc42588a62b25133867b4ff0465bdf6cccbe0'
];

class Fact {
	constructor(message, publicKey, signature) {
		this.message = message;
		this.publicKey = publicKey;
		this.signature = signature;
	}

	isTheFirstPersonsFactIn (arrayOfFacts) {
		for (var i = arrayOfFacts.length - 1; i >= 0; i--) {
			if (arrayOfFacts[i].publicKey === this.publicKey) {
		    	return false;
		    }
		}
		return true;
	}

	isSentByOwner() {
		var sig = new rsa.KJUR.crypto.Signature({ alg: 'SHA256withRSA' });
		sig.init(rsa.KEYUTIL.getKey(this.publicKey));
		sig.updateString(this.message);
		return sig.verify(this.signature);
	}

	isValid(){
		// check if the fact was sent by real owner
		if (!this.isSentByOwner()) {
			return false;
		}

		// check if there are no facts from the person in pool
		if (!this.isTheFirstPersonsFactIn(pool)) {
			return false;
		}

		// check if there are no facts from the person in chain in each block
		// skipping first (genesis) block
		for (var i = blockchain.length - 1; i >= 1; i--) {
			if (!this.isTheFirstPersonsFactIn(blockchain[i].data)) {
		    	return false;
		    }
		}
		return true;
	} 
}

class Block {
    constructor(index, previousHash, data, nonce, hash = '') {
        this.index = index;
        this.previousHash = previousHash;
        this.data = data;
        if(hash === '') {
        	this.hash = this.calculateHash();
        }
        else {
        	this.hash = hash;
        }
        this.nonce = nonce;
    }

    calculateHash() {
		return sha256(this.index + this.previousHash + JSON.stringify(this.data) + this.nonce);
	}

    isValid(previousBlock) {
    	//check index
    	if (previousBlock.index + 1 !== this.index) {
	        return false;
	    } 

	    // check previous block hash
	    if (previousBlock.hash !== this.previousHash) {
	        return false;
	    }

	    // check hash
		var calculatedHash = this.calculateHash();

	    if (calculatedHash !== this.hash) {
	        return false;
	    }

	    if(calculatedHash.slice(-4) !== '0000'){
	    	return false;
	    }
	    return true;
    }

    isGenesisBlock() {
		return this.index === 0;
	}
}

// create genesis block
var blockchain = [new Block(0, '0', dataBase, 0)];

// create empty pool
var pool = [];

io.on('connection', function(socket){
	socket.send(blockchain);

	socket.on('make vote', function(message, publicKey, signature) {
		
		var newFact = new Fact(message, publicKey, signature);
		
		// add fact to pool if the user hasnt voted before
		if (newFact.isValid()) {
			pool.push(newFact);
			socket.emit('vote success');
		} else {
			socket.emit('validationError', "Помилка! Перевірте введені дані!");
		}
  	});

	socket.on('get everything from pool', function() {
		socket.emit('mine', pool);
	});

	socket.on('add new block', function(newBlock) {

		// create new Block from newblock
		var block = new Block(newBlock.index, newBlock.previousHash, newBlock.data, newBlock.nonce, newBlock.hash);

		if (!block.isValid(blockchain[blockchain.length-1])) {
			return;
		}

		// delete used facts from pool
		var onlyInPool = pool.filter(function(current) {
		    return block.data.filter(function(current_block) {
		        return current_block.publicKey === current.publicKey;
		    }).length === 0;
		});

		var onlyInBlock = block.data.filter(function(current){
		    return pool.filter(function(current_block_data){
		        return current_block_data.publicKey === current.publicKey
		    }).length === 0
		});

		pool = onlyInPool.concat(onlyInBlock);

		// add block to blockchain
		blockchain.push(block);
		io.sockets.emit('new blockchain', blockchain);
	});

});

http.listen(3000, function(){
  console.log('listening on *:3000');
});